"""
This is the implementation of data preparation for sklearn
"""

import sys
import logging
from nltk.corpus import stopwords
from nltk.tokenize import sent_tokenize, word_tokenize

# Following two imports are required for Xpresso. Do not remove this.
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "### Author ###"

# To use the logger please provide the name and log level
#   - name is passed as the project name while generating the logs
#   - level can be DEBUG, INFO, WARNING, ERROR, CRITICAL
logger = XprLogger(name="data_prep",
                   level=logging.INFO)

STOPWORDS = ['i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves', 'you', "you're", \
            "you've", "you'll", "you'd", 'your', 'yours', 'yourself', 'yourselves', 'he', 'him', \
            'his', 'himself', 'she', "she's", 'her', 'hers', 'herself', 'it', "it's", 'its', \
            'itself', 'they', 'them', 'their', 'theirs', 'themselves', 'what', 'which', 'who', \
            'whom', 'this', 'that', "that'll", 'these', 'those', 'am', 'is', 'are', 'was', \
            'were', 'be', 'been', 'being', 'have', 'has', 'had', 'having', 'do', 'does', \
            'did', 'doing', 'a', 'an', 'the', 'and', 'but', 'if', 'or', 'because', 'as', 'until', \
            'while', 'of', 'at', 'by', 'for', 'with', 'about', 'against', 'between', 'into', \
            'through', 'during', 'before', 'after', 'above', 'below', 'to', 'from', 'up', \
            'down', 'in', 'out', 'on', 'off', 'over', 'under', 'again', 'further',\
            'then', 'once', 'here', 'there', 'when', 'where', 'why', 'how', 'all', 'any', \
            'both', 'each', 'few', 'more', 'most', 'other', 'some', 'such', 'only', 'own', \
            'same', 'so', 'than', 'too', 'very', 's', 't', 'can', 'will', 'just', 'don', \
            "don't", 'should', "should've", 'now', 'd', 'll', 'm', 'o', 're', \
            've', 'y', 'ain', 'aren', "aren't", 'couldn', "couldn't", 'didn', "didn't", \
            'doesn', "doesn't", 'hadn', "hadn't", 'hasn', "hasn't", 'haven', "haven't", \
            'isn', "isn't", 'ma', 'mightn', "mightn't", 'mustn', "mustn't", 'needn', \
            "needn't", 'shan', "shan't", 'shouldn', "shouldn't", 'wasn', "wasn't", 'weren', \
            "weren't", 'won', "won't", 'wouldn', "wouldn't"]

GROUPED_ITEM_KEYWORDS = ['bundle', 'lot ', 'pair']
VALIDITY_THRESHOLD= 25.0


class DataPrep(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self):
        super().__init__(name="DataPrep")
        """ Initialize all the required constansts and data her """

    def seperate_categories(self, df):
        # Separate category types to 3 columns. 
        df['category_name'] = df['category_name'].replace(np.nan, '//', regex=True)
        df['category1'] = "None"
        df['category2'] = "None"
        df['category3'] = "None"
        df[['category1','category2','category3']] = df['category_name'].str.split("/", n = 2, expand = True)
        df['category_name'] = df['category_name'].replace( '//',np.nan, regex=False)
        return df

    def fill_na_item_description(self, df):
        df['item_description'] = df['item_description'].fillna('No Description Yet')
        return df

    def decontracted(self, phrase):
        # specific
        phrase = str(phrase)
        phrase = re.sub(r"won't", "will not", phrase)
        phrase = re.sub(r"can\'t", "can not", phrase)

        # general
        phrase = re.sub(r"n\'t", " not", phrase)
        phrase = re.sub(r"\'re", " are", phrase)
        phrase = re.sub(r"\'s", " is", phrase)
        phrase = re.sub(r"\'d", " would", phrase)
        phrase = re.sub(r"\'ll", " will", phrase)
        phrase = re.sub(r"\'t", " not", phrase)
        phrase = re.sub(r"\'ve", " have", phrase)
        phrase = re.sub(r"\'m", " am", phrase)
        return phrase

    def clean_para(self, row):
        preprocessed_item_description = []
        sentance = row
        sent = decontracted(sentance)
        sent = sent.replace('\\r', ' ')
        sent = sent.replace('\\"', ' ')
        sent = sent.replace('\\n', ' ')
        sent = re.sub('[^A-Za-z0-9]+', ' ', sent)
        sent = ' '.join(e for e in sent.split() if e not in stopwords)
        preprocessed_item_description.append(sent.lower().strip())
        return preprocessed_item_description[0]

    def preprocess_df(self, df):
        df = seperate_categories(df)
        # df['item_description'] = df['item_description'].progress_apply(lambda x:clean_para(x))
        # df['name'] = df['name'].progress_apply(lambda x:clean_para(x))
        return df

    def get_valid_category_wise_price_std(self, df):
        unique_categories = df['category_name'].unique()
        unique_categories = list(set(unique_categories))
        unique_categories = [x for x in unique_categories if str(x) != 'nan']
        cat_std_list = []
        for cat in unique_categories:
            if str(cat) != 'nan':
                cat_std_list.append(df.loc[df['category_name']==cat][['price']].std().values[0])
        cat_std_list = [0 if math.isnan(float(x)) else x for x in cat_std_list]
        cat_price_std = dict(zip(unique_categories, cat_std_list)) 
        cat_price_std_sorted = dict(sorted(cat_price_std.items(), key=operator.itemgetter(1),reverse=True))
        valid_cat_price_dict = {}
        for k,v in cat_price_std_sorted.items():
            if v>VALIDITY_THRESHOLD:
                valid_cat_price_dict[k] = v
        return valid_cat_price_dict

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        try:
            super().start(xpresso_run_name=run_name)
            # === Your start code base goes here ===
            # TODO: Pass training dataset file
            preprocess_df(train_df)
            valid_cat_price_dict = get_valid_category_wise_price_std(train_df)
            category1_keyword_dict = defaultdict(list)
            ### $xpr_param_import_start

        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed(success=True)

    def send_metrics(self):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": "data_preparation"},
                "metric": {"metric_key": 1}
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        try:
            super().completed(push_exp=push_exp, success=success)
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py

    data_prep = DataPrep()
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
